<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>商品詳細</title>

    <link href="css/detail.css" rel="stylesheet" type="text/css" />
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<body>
    <div class="row sticky-top">
        <jsp:include page="header.jsp" />
    </div>

    <div id="container" class="container-fruid">
      <div class="row">
          <div class="col">
              <div class="row">
                  <div class="col-3 mt-5 mx-auto" style="width: 400px;">
                    <img src="img/${productInfo.imgPath}" class="img-thumbnail">
                  </div>

                  <div class="col mt-5">
                      <h3 class="my-3">${productInfo.name}</h3>
                      <h6 class="mb-5">
                         ${productInfo.detail}
                      </h6>
                      <h5 class="mb-3">価格：¥${productInfo.price}</h5>
                      <div class="d-grid gap-2 d-md-block">
                        <a class="btn btn-primary btn-sm" href="CartAddServlet?id=${productInfo.id}" role="button">カートに入れる</a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW"
        crossorigin="anonymous"></script>

</body>

</html>