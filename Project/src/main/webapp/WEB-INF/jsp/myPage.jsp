<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>マイページ</title>

    <link href="css/myPage.css" rel="stylesheet" type="text/css" />
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<body>
    <div class="row sticky-top">
        <jsp:include page="header.jsp" />
    </div>

    <div id="container" class="container-fruid">
        <div class="row">
            <h4 class="my-3 p-3 border-bottom">お客様情報</h4>
            <form class="col-10 mx-auto" action="UserUpdateServlet" method="post">
                <div class="row mb-3">
                	<label for="inputEmail3" class="col-sm-2 col-form-label">氏名</label>
                	<div class="col-sm-10">
                    <input type="text" class="form-control" name="name" value="${userInfo.name}">
                	</div>
                </div>
                <div class="row mb-3">
                	<label for="inputPassword3" class="col-sm-2 col-form-label">E-mail</label>
                	<div class="col-sm-10">
                    <input type="email" class="form-control" name="email" value="${userInfo.email}">
                	</div>
                </div>
                <div class="row mb-3">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">住所</label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" name="adress" value="${userInfo.adress}">
                    </div>
                </div>
                <div class="d-grid gap-2 d-md-block">
                        <button class="btn btn-primary" type="submit">更新</button>
                        <a class="btn btn-light" href="UserDeleteServlet">退会する</a>
                </div>
            </form>
        </div>

        <div class="row">
            <h4 class="my-5 p-3 border-bottom">購入履歴</h4>
            <div class="mx-auto p-3">
                <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">購入日時</th>
                        <th scope="col">配送方法</th>
                        <th scope="col">合計金額</th>
                        <th scope="col"></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1234年56月78日00時00分</td>
                        <td>通常配送</td>
                        <td>¥123456</td>
                        <td><button type="button" class="btn btn-primary">詳細</button></td>
                      </tr>
                      <tr>
                        <td>1234年56月78日00時00分</td>
                        <td>特急配送</td>
                        <td>¥123456</td>
                        <td><button type="button" class="btn btn-primary">詳細</button></td>
                      </tr>
                      <tr>
                        <td>1234年56月78日00時00分</td>
                        <td>日時指定配送</td>
                        <td>¥123456</td>
                        <td><button type="button" class="btn btn-primary">詳細</button></td>
                      </tr>
                    </tbody>
                  </table>
            </div>
        </div>
        <div class="d-grid gap-2 col-4 mx-auto my-5">
            <button class="btn btn-secondary" type="button">ページトップへ</button>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

</body>

</html>