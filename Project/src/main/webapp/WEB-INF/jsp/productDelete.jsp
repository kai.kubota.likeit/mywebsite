<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
 <meta name="viewport" content="width=device-width, initial-scale=1">
<title>商品の削除</title>

	<link href="css/header.css" rel="stylesheet" type="text/css" />
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<body>
	<div class="row sticky-top">
        <jsp:include page="header.jsp" />
    </div>
	
	<div id="container" class="container-fruid">
        <div class="row">
            <h4 class="my-3 p-3 border-bottom">この商品を削除しますか？</h4>
        </div>

        <div class="col-3 mt-5 mx-auto" style="width: 400px;">
            <img src="img/${product.imgPath}" class="img-thumbnail">
        </div>

        <div>
            <form action="ProductDeleteServlet" method="post">
                <p>商品名：${product.name}</p>
                <p>価格：${product.price}</p>
                <p>カテゴリ：${product.categoryName}</p>
                <p>メーカー：${product.companyName}</p>
                <input type="hidden" name="id" value="${product.id}">
                <button class="btn btn-primary me-md-2" type="submit">削除</button>
                <button class="btn btn-secondary" type="button">戻る</button>
            </form>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW"
        crossorigin="anonymous"></script>
</body>
</html>