<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>商品検索結果</title>

    <link href="css/searchResult.css" rel="stylesheet" type="text/css" />
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<body>

    <div class="row sticky-top">
        <jsp:include page="header.jsp" />
    </div>

    <div id="container" class="container-fruid">
        <div class="row">
            <div class="col mt-3">
                <div class="row">
                    <div class="col-2 mx-auto my-3">
                        <h4>検索結果</h4>
                    </div>
            
                    <div id="button-group">
                        <div class="dropdown">
                            <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                            並べ替え
                            </a>
                        
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <li><a class="dropdown-item" href="#">人気順</a></li>
                            <li><a class="dropdown-item" href="#">発売日順</a></li>
                            <li><a class="dropdown-item" href="#">価格が安い順</a></li>
                            <li><a class="dropdown-item" href="#">価格が高い順</a></li>
                            </ul>
                        </div>
            
                        <div class="dropdown">
                            <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                            カテゴリ
                            </a>
                        
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <li><a class="dropdown-item" href="#">マウス</a></li>
                            <li><a class="dropdown-item" href="#">キーボード</a></li>
                            <li><a class="dropdown-item" href="#">ヘッドホン・イヤホン</a></li>
                            <li><a class="dropdown-item" href="#">PCモニター</a></li>
                            <li><a class="dropdown-item" href="#">マウスパッド</a></li>
                            <li><a class="dropdown-item" href="#">アクセサリー・その他</a></li>
                            </ul>
                        </div>
            
                        <div class="dropdown">
                            <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
                            メーカー
                            </a>
                        
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <li><a class="dropdown-item" href="#">Logicool</a></li>
                            <li><a class="dropdown-item" href="#">Razer</a></li>
                            <li><a class="dropdown-item" href="#">SteelSeries</a></li>
                            <li><a class="dropdown-item" href="#">Corsair</a></li>
                            <li><a class="dropdown-item" href="#">BenQ</a></li>
                            <li><a class="dropdown-item" href="#">ASUS</a></li>
                            <li><a class="dropdown-item" href="#">Ducky</a></li>
                            <li><a class="dropdown-item" href="#">SHURE</a></li>
                            </ul>
                        </div>
                    </div>
            
                    <div class="row mt-3">
	                    <c:forEach var="product" items="${productList}">
	                        <div class="col s12 m3">
	                            <div class="card">
	                                <div class="card-image">
	                                    <a href="productDetailServlet?id=${product.id}"><img src="img/${product.imgPath}"></a>
	                                </div>
	                                <div class="card-content">
	                                    <span class="card-title">${product.name}</span>
	                                    <p>${product.price}</p>
	                                </div>
	                            </div>
	                        </div>
	                    </c:forEach>
                    </div>
                    <div class="col my-5 text-center">
                        <a href="header">ページ上に戻る</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    
</body>

</html>