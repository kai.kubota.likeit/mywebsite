<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>商品の更新</title>

    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        
</head>
   	<div class="row sticky-top">
        <jsp:include page="header.jsp" />
    </div>

    <div id="container" class="container-fruid">
        <div class="row">
            <h4 class="my-3 p-3 border-bottom">商品情報更新ページ</h4>
        </div>

        <form class="col-8 mx-auto" action="ProductUpdateServlet" method="post">
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">商品名</label>
                <input type="text" class="form-control" name="name" value="${product.name}">
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">商品価格</label>
                <input type="text" class="form-control" name="price" value="${product.price}">
            </div>
            <div class="mb-3">
                <select class="form-select" aria-label="productCategory" name="categoryId">
                        <option selected value="${product.categoryId}">${product.categoryName}</option>
                        <c:forEach var="category" items="${categoryList}">
                        	<option value="${category.categoryId}">${category.categoryName}</option>
                        </c:forEach>
                </select>
            </div>
            <div class="mb-3">
                <select class="form-select" aria-label="productCompany" name="companyId">
                        <option selected value="${product.companyId}">${product.companyName}</option>
                        <c:forEach var="company" items="${companyList}">
                        	<option value="${company.companyId}">${company.companyName}</option>
                        </c:forEach>
                </select>
            </div>
            <div class="mb-3">
                <label for="exampleFormControlTextarea1" class="form-label">商品詳細</label>
                <textarea class="form-control" name="detail" rows="3">${product.detail}</textarea>
            </div>
            <div class="mb-3">
                <label for="exampleFormControlInput1" class="form-label">商品画像の保存先</label>
                <input type="text" class="form-control" name="imgPath" value="${product.imgPath}">
            </div>
            <div class="d-grid gap-2 d-md-flex justify-content-md-end mb-5">
            	<input type="hidden" name="id" value="${product.id}">
                <button class="btn btn-primary me-md-2" type="submit">更新</button>
                <button class="btn btn-secondary" type="button">戻る</button>
            </div>  
        </form>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW"
        crossorigin="anonymous"></script>
<body>

</body>
</html>