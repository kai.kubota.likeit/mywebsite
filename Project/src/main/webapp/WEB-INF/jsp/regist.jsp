<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>会員登録</title>

    <link href="css/regist.css" rel="stylesheet" type="text/css" />
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<body>
    <div class="row sticky-top">
        <jsp:include page="header.jsp" />
    </div>

    <div id="container" class="container-fruid">
        <div class="row">
            <h4 class="my-3 p-3 border-bottom">新規会員登録</h4>
            <form class="col-10 mx-auto" action="RegistServlet" method="post">
                <div class="row mb-3">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" name="email">
                </div>
                </div>
                <div class="row mb-3">
                <label for="inputname3" class="col-sm-2 col-form-label">名前</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name">
                </div>
                </div>
                <div class="row mb-3">
                    <label for="inputadress3" class="col-sm-2 col-form-label">住所</label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" name="adress">
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">パスワード</label>
                    <div class="col-sm-10">
                    <input type="password" class="form-control" name="password">
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="inputPassword3" class="col-sm-2 col-form-label">パスワード(確認)</label>
                    <div class="col-sm-10">
                    <input type="password" class="form-control" name="password2">
                    </div>
                </div>
                <div class="d-grid gap-2 d-md-flex justify-content-md-end mb-5">
                    <button class="btn btn-primary me-md-2" type="submit">確認</button>
                    <button class="btn btn-secondary" type="button">戻る</button>
                </div>  
            </form>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

</body>

</html>