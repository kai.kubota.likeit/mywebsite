<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>会員登録(確認)</title>

    <link href="css/registConfirm.css" rel="stylesheet" type="text/css" />
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<body>
    <div class="row sticky-top">
        <jsp:include page="header.jsp" />
    </div>
    
    <div id="container" class="container-fruid">
        <div class="row">
            <div class="col mt-3">
                <h4 class="my-3 p-3 border-bottom">登録内容確認</h4>
                <h6>この情報で会員登録を行います。よろしいですか？</h6>

                <form class="my-3 p-5" action="RegistConfirmServlet" method="post">
                    <p>メールアドレス：${registInfo.email}</p>
                    <p>氏名：${registInfo.name}</p>
                    <p>住所：${registInfo.adress}</p>
                    <p>パスワード：${registInfo.password}</p>
                    <div class="d-grid gap-2 d-md-flex justify-content-md-end my-5">
                        <button class="btn btn-primary me-md-2" type="submit">登録</button>
                        <button class="btn btn-secondary" type="button">戻る</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    
</body>

</html>