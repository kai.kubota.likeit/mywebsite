<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>購入履歴詳細</title>

    <link href="css/buyDetail.css" rel="stylesheet" type="text/css" />
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<body>
    <div class="row sticky-top">
        <jsp:include page="header.jsp" />
    </div>

    <div id="container" class="container-fruid">
        <div class="row">
            <h4 class="mb-3 p-3 border-bottom">購入履歴詳細</h4>
        </div>
        <div class="border mx-auto" style="padding:30px;">
            <div class="row pb-5">
                <div class="col">
                    <img src="img/logicool_g910r.jpeg" class="img-thumbnail" alt="G910r">
                </div>
                <div class="col">
                    <p>Logicool G910r</p>
                    <p>数量：1</p>
                </div>
                <div class="col">
                    <p>¥23,000</p>
                </div>
            </div>
    
            <div class="row pb-5">
                <div class="col">
                    <img src="img/logicool_g910r.jpeg" class="img-thumbnail" alt="G910r">
                </div>
                <div class="col">
                    <p>Logicool G910r</p></a>
                    <p>数量：1</p>
                </div>
                <div class="col">
                    <p>¥23,000</p>
                </div>
            </div>
            
            <div class="border-top " style="padding:10px;">
                <div class="col">
                    <p>配送方法：特急配送</p>
                </div>
                <div class="col">
                    <p>¥300</p>
                </div>
            </div>

            <div class="border-top" style="padding:10px;">
                <a>小計 ¥46,300</a>
            </div>

            </div>

            <div class="border mx-auto mt-3 mb-5" style="padding:30px;">
                <div class="row">
                    氏名：山田太郎
                </div>
            <div class="row">
                お届け先住所：東京都
            </div>
            </div>

            <div class="mx-auto">
                <button type="button" class="btn btn-secondary">戻る</button>
            </div>

            <div class="col my-5 text-center">
                <a href="header">ページ上に戻る</a>
            </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

</body>

</html>