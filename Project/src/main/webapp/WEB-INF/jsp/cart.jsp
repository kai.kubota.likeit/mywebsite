<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>カート</title>

    <link href="css/cart.css" rel="stylesheet" type="text/css" />
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<body>

    <div class="row sticky-top">
        <jsp:include page="header.jsp" />
    </div>

    <div id="container" class="container-fruid">
        <div class="row">
            <div class="col">
            	<div class="row center">
					${cartActionMessage}
				</div>
                <div class="col-2 mx-auto py-5">
                    <h3 class="ml-4">カート</h3>
                </div>
        		
        		<c:forEach var="product" items="${cart}" varStatus="status">
	                <div class="row pb-5">
	                    <div class="col">
	                        <a href="CartServlet?id=${product.id}"><img src="${product.imgPath}" class="img-thumbnail"></a>
	                    </div>
	                    <div class="col">
	                        <a href="CartServlet?id=${product.id}">${product.name}</a>
	                    </div>
	                    <div class="col">
	                        <p>${product.price}</p>
	                        <button class="btn btn-primary" type="button" onclick="location.href='cart.html'">カートから削除</button>
	                    </div>
	                </div>
                </c:forEach>
        
                <div class="col-2 mx-auto pb-5">
                    <a href="BuyServlet" class="btn btn-primary">レジへ進む</a>
                </div>
            </div>
        </div>
    </div>
    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

</body>

</html>