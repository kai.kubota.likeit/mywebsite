<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>商品追加ページ</title>

    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<body>
    <div class="row sticky-top">
        <jsp:include page="header.jsp" />
    </div>

    <div id="container" class="container-fruid">
        <div class="row">
            <div class="col-5 mt-3">
                <h4>商品登録ページ</h4>
            </div>
            <c:if test="${errMsg != null}" >
	    		<div class="alert alert-danger" role="alert">
		 		 	${errMsg}
				</div>
			</c:if>
            <form class="col-8 mx-auto" action="ProductAddServlet" method="post">
                <div class="mb-3">
                    <label for="productName" class="form-label">商品名</label>
                    <input type="text" class="form-control" name="name" placeholder="商品名を入力">
                </div>
                <div class="mb-3">
                    <label for="productPrice" class="form-label">商品価格</label>
                    <input type="number" class="form-control" name="price" placeholder="商品価格を入力 例) ¥2,300 → 2300">
                </div>
                <div class="mb-3">
                    <select class="form-select" aria-label="productCategory" name="categoryId">
                        <option selected>カテゴリを選択</option>
                        <c:forEach var="category" items="${categoryList}">
                        	<option value="${category.categoryId}">${category.categoryName}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="mb-3">
                    <select class="form-select" aria-label="productCompany" name="companyId">
                        <option selected>メーカーを選択</option>
                        <c:forEach var="company" items="${companyList}">
                        	<option value="${company.companyId}">${company.companyName}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="productDetail" class="form-label">商品詳細</label>
                    <textarea class="form-control" name="detail" rows="3"></textarea>
                </div>
                <div class="mb-3">
                    <label for="imagePath" class="form-label">商品画像の保存先</label>
                    <input type="text" class="form-control" name="imgPath" placeholder="商品画像の保存先を入力">
                </div>
                <div class="d-grid gap-2 d-md-flex justify-content-md-end mb-5">
                    <button class="btn btn-primary me-md-2" type="submit">登録</button>
                    <button class="btn btn-secondary" type="button">戻る</button>
                </div>  
            </form>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>