<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ホーム</title>

    <link href="css/home.css" rel="stylesheet" type="text/css" />
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<body>
    <div class="row sticky-top">
        <jsp:include page="header.jsp" />
    </div>
    
    <div id="container" class="container-fruid">
        <div class="row mt-2 p-1">
          <h4 class="mb-3">売れ筋ランキング</h4>
          <c:forEach var="newItem" items="${newItems}">
          	<div class="col">
              <a href="ProductDetailServlet?id=${newItem.id}"><img src="img/${newItem.imgPath}" class="img-thumbnail"></a>
          	</div>
          </c:forEach>
          <div class="mt-2">
            <a href="searchResult.html">ランキングを見る</a>
          </div>
        </div>

        <div class="row mt-2 p-1">
          <div class="col"></div>
          <h4 class="mb-3">新着商品</h4>
          <c:forEach var="newItem" items="${newItems}">
          	<div class="col">
              <a href="ProductDetailServlet?id=${newItem.id}"><img src="img/${newItem.imgPath}" class="img-thumbnail"></a>
          	</div>
          </c:forEach>
          <div class="mt-2">
            <a href="searchResult.html">新着商品を見る</a>
          </div>
        </div>

        <div class="row row-cols-1 row-cols-md-3 g-4 mt-3 p-3">
          <div class="col">
            <div class="card h-100">
              <a href=""><img src="img/Apex_image.jpeg" class="card-img-top" alt="apex"></a>
              <div class="card-body">
                <h5 class="card-title">Apex Legendsおすすめデバイス</h5>
                <p class="card-text">プロやストリーマーも使用しているApex Legends向けのデバイスをご紹介！</p>
                <a href="#" class="btn btn-outline-primary">紹介ページへ</a>
              </div>
            </div>
          </div>
          <div class="col">
            <div class="card h-100">
              <a href=""><img src="img/Valorant_image.jpeg" class="card-img-top" alt="valorant"></a>
              <div class="card-body">
                <h5 class="card-title">Valorantおすすめデバイス</h5>
                <p class="card-text">プロやストリーマーも使用しているValorant向けのデバイスをご紹介！</p>
                <a href="#" class="btn btn-outline-primary">紹介ページへ</a>
              </div>
            </div>
          </div>
          <div class="col">
            <div class="card h-100">
              <a href=""><img src="img/Fortnite_image.jpeg" class="card-img-top" alt="fortnite"></a>
              <div class="card-body">
                <h5 class="card-title">Fortniteおすすめデバイス</h5>
                <p class="card-text">プロやストリーマーも使用しているFortnite向けのデバイスをご紹介！</p>
                <a href="#" class="btn btn-outline-primary">紹介ページへ</a>
              </div>
            </div>
          </div>
        </div>

        <div class="d-grid gap-2 col-4 mx-auto my-5">
          <button class="btn btn-secondary" type="button">ページトップへ</button>
        </div>

      </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

</body>

</html>