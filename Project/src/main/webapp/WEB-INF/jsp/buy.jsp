<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>購入画面</title>

    <link href="css/buy.css" rel="stylesheet" type="text/css" />
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<body>
    <div class="row sticky-top">
       <jsp:include page="header.jsp" />
    </div>

    <div id="container" class="container-fruid">
        <div class="row">
            <div class="col mt-5">
                <div class="col-2 mx-auto mb-5">
                    <h4 class="ml-4">購入する</h4>
                </div>

                <div class="border mx-auto" style="padding:30px;">
                	<c:forEach var="product" items="${cart}" varStatus="status">
	                    <div class="row pb-5">
	                        <div class="col">
	                            <a href="ProductDetailServlet?id=${product.id}"><img src="${product.imgPath}" class="img-thumbnail"></a>
	                        </div>
	                        <div class="col">
	                            <a href="ProductDetailServlet?id=${product.id}">
	                                ${product.name}
	                            </a>
	                        </div>
	                        <div class="col">
	                            <p>${product.price}</p>
	                            <button class="btn btn-primary" type="button"
	                                onclick="location.href='cart.html'">商品を削除</button>
	                        </div>
	                    </div>
                    </c:forEach>

                    <div class="pull-right border-top " style="padding:10px;">
                        <div class="input-field col s8 offset-s2 ">
                            <label>配送方法</label>
                            <select name="method">
                            <c:forEach var="method" items="${methodList}" varStatus="status">
                                <option value="${method.id}">${method.name}</option>
                            </c:forEach>
                            </select>
                        </div>
                    </div>

                    <div class="border-top text-right" style="padding:10px;">
                        <a>小計 ¥46,300</a>
                    </div>
                </div>

                <!-- ログインセッションがない場合は氏名・住所は入力なし -->
                <div class="border mx-auto mt-3 mb-5" style="padding:30px;">
                    <div class="row mb-4 col-10 mx-auto">
                        <label>氏名</label>
                        <c:if test="${userInfo == null}">
                        	<input class="ml-3" type="text" name="name">
                        </c:if>
                        <c:if test="${userInfo != null}">
                        	<input class="ml-3" type="text" name="name" value="${userInfo.name}">
                        </c:if>
                    </div>
                    <div class="row mb-4 col-10 mx-auto">
                        <label>お届け先住所</label>
                      	<c:if test="${userInfo == null}">
                        	<input class="ml-3" type="text" name="adress">
                        </c:if>
                        <c:if test="${userInfo != null}">
                        	<input class="ml-3" type="text" name="adress" value="${userInfo.adress}">
                        </c:if>
                    </div>
                </div>

                <div class="d-grid gap-2 d-md-flex justify-content-md-end ">
                    
                    <button class="btn btn-secondary" type="button">キャンセル</button>
                </div>

                <div class="col my-5 text-center">
                    <a href="header">ページトップへ戻る</a>
                </div>
            </div>
        </div>
	</div>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW"
            crossorigin="anonymous"></script>

</body>

</html>