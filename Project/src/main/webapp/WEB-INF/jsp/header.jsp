<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ホーム</title>

    <link href="css/home.css" rel="stylesheet" type="text/css" />
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
      <a href="HomeServlet" class="navbar-brand">FTW.GG</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a href="HomeServlet" class="nav-link">ホーム</a>
          </li>
          <li class="nav-item">
            <a href="CartServlet" class="nav-link">カート</a>
          </li>
          <li class="nav-item">
          	<c:if test="${userInfo == null}">
          			<a href="LoginServlet" class="nav-link">ログイン</a>
          	</c:if>
          	<c:if test="${userInfo != null}">
          			<a href="MyPageServlet" class="nav-link">マイページ</a>
          	</c:if>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              カテゴリ
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="#">マウス</a></li>
                <li><a class="dropdown-item" href="#">キーボード</a></li>
                <li><a class="dropdown-item" href="#">イヤホン・ヘッドホン</a></li>
                <li><a class="dropdown-item" href="#">マウスパッド</a></li>
                <li><a class="dropdown-item" href="#">アクセサリ・その他</a></li>
            </ul>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              メーカー
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="#">Logicool</a></li>
                <li><a class="dropdown-item" href="#">Razer</a></li>
                <li><a class="dropdown-item" href="#">Steel Series</a></li>
                <li><a class="dropdown-item" href="#">Corsair</a></li>
                <li><a class="dropdown-item" href="#">BenQ</a></li>
                <li><a class="dropdown-item" href="#">ASUS</a></li>
                <li><a class="dropdown-item" href="#">SHURE</a></li>
                <li><a class="dropdown-item" href="#">Ducky</a></li>
            </ul>
          </li>
          <li class="nav-item">
          	<c:if test="${userInfo.name == 'admin'}">
          		<li class="nav-item dropdown">
            		<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              			管理者メニュー
            		</a>
            		<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                		<li><a class="dropdown-item" href="ProductAddServlet">商品登録</a></li>
                		<li><a class="dropdown-item" href="ProductMasterServlet">商品管理</a></li>
            		</ul>
          		</li>
          	</c:if>
          </li>
          <li class="nav-item">
          	<c:if test="${userInfo != null}">
          			<a href="LogoutServlet" class="nav-link">ログアウト</a>
          	</c:if>
          </li>
          <li class="nav-item">
          		<c:if test="${userInfo.name == null}">
          			<a href="LoginServlet" class="nav-link active" aria-current="page">ようこそ、ゲストさん</a>
          		</c:if>
          		<c:if test="${userInfo.name != null}">
          			<a href="MypageServlet"class="nav-link active" aria-current="page">ようこそ、${userInfo.name}さん</a>
          		</c:if>
          </li>
        </ul>
        <form class="d-flex" action="SearchResultServlet" method="post">
          <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search" name="searchWord">
          <button class="btn btn-outline-success" type="submit">Search</button>
        </form>
      </div>
    </div>
  </nav>
</body>