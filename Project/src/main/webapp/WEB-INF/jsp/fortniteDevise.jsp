<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Fortniteデバイス</title>

    <link href="css/fortniteDevice.css" rel="stylesheet" type="text/css" />
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<body>
    <div class="row sticky-top">
        <jsp:include page="header.jsp" />
    </div>

    <div id="container" class="container-fruid">
        <div class="row">
            <div class="col mt-3">
                <div class="col border-bottom">
                    <h3>Fortnite向けのおすすめデバイス</h3>
                    <p>大会で活躍するプロやストリーマーが使用しているデバイスを厳選。</p>
                </div>
        
                <div id="carouselExampleCaptions" class="carousel slide mx-auto mt-5" data-bs-ride="carousel">
                    <ol class="carousel-indicators">
                      <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active"></li>
                      <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"></li>
                      <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                      <div class="carousel-item active">
                        <a href="#cr_page"><img src="img/CrazyRaccoon_logo.png" class="d-block w-100" alt="cr"></a>
                        <div class="carousel-caption d-none d-md-block">
                          <h5>Crazy Raccoon所属選手</h5>
                          <p>CR所属の選手たちのデバイスをご紹介！</p>
                        </div>
                      </div>
                      <div class="carousel-item">
                        <a href="#riddle_page"><img src="img/Riddle_logo.jpeg" class="d-block w-100" alt="riddle"></a>
                        <div class="carousel-caption d-none d-md-block">
                          <h5>Riddle所属選手</h5>
                          <p>Riddle所属の選手たちのデバイスをご紹介！</p>
                        </div>
                      </div>
                      <div class="carousel-item">
                        <a href="#streamer_page"><img src="img/Streamer_image.jpeg" class="d-block w-100" alt="streamer"></a>
                        <div class="carousel-caption d-none d-md-block">
                          <h5>ストリーマー</h5>
                          <p>ストリーマーたちのデバイスをご紹介！</p>
                        </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                        </a>
                    </div>
                </div>
        
                <div id="cr_page" class="row border-bottom mt-5">
                    <h3>Crazy Raccoon所属選手</h3>
                </div>
        
                <div class="row mt-5">
                    <div class="col-4">
                        <p>
                            リズアート選手
                        </p>
                        <img src="img/Fortnite_RizArt.jpeg">
                    </div>
                    <div class="col-8">
                        <p>
                            若干16歳にして「日本e-Sports界の至宝」と謳われる実力を持つリズアート選手。
                            実力は間違いないものであり、何よりまだ彼自身高校生であるため、これからの活躍に大変期待できる未来のアスリートの一人だ。
        
                        </p>
                        <p>
                            主な実績
                            ・YouTube登録者数：400,000人
                            ・Twitterフォロワー154,000人
                            ・ソロモード キル数世界一位記録保持
                            ・FNCS Grand Final seasonX チャンピオン
                        </p>
                    </div>
                    
                    <div class="col">
                        <p>マウス　Logicool G502</p>
                        <img src="##">
                    </div>
                    <div class="col">
                        <p>ヘッドホン　ASTRO A40TR</p>
                        <img src="##">
                    </div>
                    <div class="col">
                        <p>キーボード　SteelSeries Apex Pro TKL</p>
                        <img src="##">
                    </div>
                </div>
        
                <div class="row mt-5">
                    <div class="col-4">
                        <p>
                            スカーレット選手
                        </p>
                        <img src="img/Fortnite_Scarlet.jpeg">
                    </div>
                    <div class="col-8">
                        <p>
                            主な実績
                            ・YouTube登録者数：100,000人
                            ・Twitterフォロワー数：42,000人
                            ・スクアッドモード キル数アジア世界記録保持者
                            ・FNCS Grand Final season1 5位
                        </p>
                    </div>
                    
                    <div class="col">
                        <p>マウス　Logicool G PRO</p>
                        <img src="##">
                    </div>
                    <div class="col">
                        <p>ヘッドホン　SHURE SE315</p>
                        <img src="##">
                    </div>
                    <div class="col">
                        <p>キーボード　Logicool G913</p>
                        <img src="##">
                    </div>
                </div>
        
                <div id="riddle_page" class="row border-bottom mt-5">
                    <h3>Riddle所属選手</h3>
                </div>
        
                <div class="row mt-5">
                    <div class="col-4">
                        <p>
                            まうふぃん選手
                        </p>
                        <img src="img/Fortnite_Maufin.jpeg">
                    </div>
                    <div class="col-8">
                        <p>
                            主な実績
                            ・YouTube登録者数：370,000人
                            ・Twitterフォロワー数：240,000人
                            ・FNCS Grand Final アジア1位
                            ・Fortniteワールドカップ決勝 出場
                        </p>
                    </div>
                    
                    <div class="col">
                        <p>マウス　Logicoool G703h</p>
                        <img src="##">
                    </div>
                    <div class="col">
                        <p>ヘッドホン　SteelSeries Arctis 7</p>
                        <img src="##">
                    </div>
                    <div class="col">
                        <p>キーボード SteelSeries Apex Pro TKL</p>
                        <img src="##">
                    </div>
                </div>
        
                <div class="row mt-5">
                    <div class="col-4">
                        <p>
                            ぼぶくん選手
                        </p>
                        <img src="img/Fortnite_Scarlet.jpeg">
                    </div>
                    <div class="col-8">
                        <p>
                            主な実績
                            ・YouTube登録者数：220,000人
                            ・Twitterフォロワー数：110,000人
                            ・FNCS Grand Final(トリオ) アジア1位
                            ・FNCS Grand Final(スクワッド) アジア1位
                        </p>
                    </div>
                    
                    <div class="col">
                        <p>コントローラー SCUF INFINITY PRO</p>
                        <img src="##">
                    </div>
                    <div class="col">
                        <p>ヘッドホン Logicool G213</p>
                        <img src="##">
                    </div>
        
                </div>
        
                <div id="streamer_page" class="row border-bottom mt-5">
                    <h3>海外ストリーマー</h3>
                </div>
        
                <div class="row mt-5">
                    <div class="col-4">
                        <p>
                            Ninja
                        </p>
                        <img src="img/Fortnite_Ninja_2.jpeg">
                    </div>
                    <div class="col-8">
                        <p>
                            言わずと知れた世界を代表するストリーマー・Ninja。
                            Fortniteをはじめ、PUBGやHALOなど様々なゲームで視聴者を楽しませている。
                        </p>
                        <p>
                            主な実績
                            ・YouTube登録者数：24,100,000人
                            ・Twitterフォロワー数：6,750,000人
                            ・Gamescom PUBG Invitational Squad 1位
                            ・Fortnite Summer Skirmish 1位
                        </p>
                    </div>
                    
                    <div class="col">
                        <p>マウス Finalmouse Air58 Ninja</p>
                        <img src="##">
                    </div>
                    <div class="col">
                        <p>ヘッドホン beyerdynamic DT990 PRO</p>
                        <img src="##">
                    </div>
                    <div class="col">
                        <p>キーボード Corsair K70 RGB MK.2 Rapidfire</p>
                        <img src="##">
                    </div>
                </div>
                <div class="col my-5 text-center">
                    <a href="header">ページ上に戻る</a>
                </div>
            </div>
        </div>
    </div> 

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

</body>

</html>