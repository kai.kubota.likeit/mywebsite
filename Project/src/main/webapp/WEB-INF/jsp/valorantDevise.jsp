<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Valorantデバイス</title>

    <link href="css/valorantDevice.css" rel="stylesheet" type="text/css" />
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<body>
    <div class="row sticky-top">
        <jsp:include page="header.jsp" />
    </div>

    <div id="container" class="container-fruid">
        <div class="row">
            <div class="col mt-3">
                <div class="col border-bottom">
                    <h3>Valorant向けのおすすめデバイス</h3>
                    <p>大会で活躍するプロやストリーマーが使用しているデバイスを厳選。</p>
                </div>
        
                <div id="carouselExampleCaptions" class="carousel slide mx-auto mt-5" data-bs-ride="carousel">
                    <ol class="carousel-indicators">
                      <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active"></li>
                      <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"></li>
                      <li data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                      <div class="carousel-item active">
                        <a href="#cr_page"><img src="img/CrazyRaccoon_logo.png" class="d-block w-100" alt="cr"></a>
                        <div class="carousel-caption d-none d-md-block">
                          <h5>Crazy Raccoon所属選手</h5>
                          <p>CR所属の選手たちのデバイスをご紹介！</p>
                        </div>
                      </div>
                      <div class="carousel-item">
                        <a href="#zeta_page"><img src="img/ZetaDivision_image.jpeg" class="d-block w-100" alt="zeta"></a>
                        <div class="carousel-caption d-none d-md-block">
                          <h5>Zeta Division所属選手</h5>
                          <p>ZETA所属の選手たちのデバイスをご紹介！</p>
                        </div>
                        </div>
                      
                      <div class="carousel-item">
                        <a href="#streamer_page"><img src="img/Streamer_image.jpeg" class="d-block w-100" alt="streamer"></a>
                        <div class="carousel-caption d-none d-md-block">
                          <h5>ストリーマー</h5>
                          <p>ストリーマーたちのデバイスをご紹介！</p>
                        </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                        </a>
                    </div>
                </div>
        
                <div id="cr_page" class="row border-bottom mt-5">
                    <h3>Crazy Raccoon所属選手</h3>
                </div>
        
                <div class="row mt-5">
                    <div class="col-4">
                        <p>
                            Munchkin選手
                        </p>
                        <img src="img/Valorant_Munchkin.jpeg">
                    </div>
                    <div class="col-8">
                        <p>
                            オーバーウォッチリーグ最前線でプレーしていた本物のスタープレーヤー。
                            勝負強さ、圧倒的なエイム、全てが世界最高峰。
                        </p>
                    </div>
                    
                    <div class="col">
                        <p>マウス Logicool Gpro wireless SUPERLIGHT</p>
                        <img src="##">
                    </div>
                    <div class="col">
                        <p>ヘッドホン　Logicool G PRO X</p>
                        <img src="##">
                    </div>
                    <div class="col">
                        <p>キーボード　Logicool G PRO X</p>
                        <img src="##">
                    </div>
                </div>
        
                <div class="row mt-5">
                    <div class="col-4">
                        <p>
                            Neth選手
                        </p>
                        <img src="img/Valorant_Neth.jpeg">
                    </div>
                    <div class="col-8">
                        <p>
                            元CSGOトッププロプレイヤー。
                            チャーミングなルックスからは想像できない圧倒的なAIM力は他の追随を許さない。
                            確実に敵を排除していくアサシン。
                        </p>
                    </div>
                    
                    <div class="col">
                        <p>マウス　Logicool Gpro wireless SUPERLIGHT</p>
                        <img src="##">
                    </div>
                    <div class="col">
                        <p>ヘッドホン　Logicool G PRO X</p>
                        <img src="##">
                    </div>
                    <div class="col">
                        <p>キーボード　Logicool G913</p>
                        <img src="##">
                    </div>
                </div>
        
                <div class="row mt-5">
                    <div class="col-4">
                        <p>
                            Medusa選手
                        </p>
                        <img src="img/Valorant_Medusa.jpeg">
                    </div>
                    <div class="col-8">
                        <p>
                            フォートナイト元アジアトッププレイヤーとして君臨していた。
                            CSGOでは韓国トップチームでのプレイ経験がある。
                            エイムが神がかっている、なんでもできる最強の男。
                        </p>
                    </div>
                    
                    <div class="col">
                        <p>マウス　Logicool Gpro wireless SUPERLIGHT</p>
                        <img src="##">
                    </div>
                    <div class="col">
                        <p>ヘッドホン　Logicool G PRO X</p>
                        <img src="##">
                    </div>
                    <div class="col">
                        <p>キーボード　Logicool G PRO X</p>
                        <img src="##">
                    </div>
                </div>
        
                <div id="zeta_page" class="row border-bottom mt-5">
                    <h3>Zeta Division所属選手</h3>
                </div>
        
                <div class="row mt-5">
                    <div class="col-4">
                        <p>
                            Laz選手
                        </p>
                        <img src="img/Valorant_Laz.jpeg">
                    </div>
                    <div class="col-8">
                        <p>
                            世界が認める⽇本のナンバーワンプレイヤー。
                            とてつもない練習量に裏付けられた精密なAIMが数々の⼤会でスーパープレイを⽣み出し、
                            ⽇本のVALORANTシーンを引っ張るトップランナー。
                            甘いマスクでファンへ癒しも届ける配信が人気。たまにお茶目な部分も。
                        </p>
                    </div>
                    
                    <div class="col">
                        <p>マウス　Logicool G PRO Wireless</p>
                        <img src="##">
                    </div>
                    <div class="col">
                        <p>ヘッドホン　Logicool G PRO X</p>
                        <img src="##">
                    </div>
                    <div class="col">
                        <p>キーボード　Logicool G913</p>
                        <img src="##">
                    </div>
                </div>
        
                <div class="row mt-5">
                    <div class="col-4">
                        <p>
                            Reita選手
                        </p>
                        <img src="img/Valorant_Reita.jpeg">
                    </div>
                    <div class="col-8">
                        <p>
                            ⾒るものを虜にする華麗なスナイパー捌きで相⼿はやられていることにすら気がつかない。
                            実況者をも魅了し、呼び名はスナイパーライフルの名称から”オペReita”。
                            オフライン会場でのファンサービスにも定評があり、⼀部ではファンサ神と呼ばれているとか。
                        </p>
                    </div>
                    
                    <div class="col">
                        <p>マウス　Logicool G703h</p>
                        <img src="##">
                    </div>
                    <div class="col">
                        <p>ヘッドホン　Logicool G PRO X</p>
                        <img src="##">
                    </div>
                    <div class="col">
                        <p>キーボード　Logicool G913</p>
                        <img src="##">
                    </div>
                </div>
        
                <div class="row mt-5">
                    <div class="col-4">
                        <p>
                            Crow選手
                        </p>
                        <img src="img/Valorant_Crow.jpeg">
                    </div>
                    <div class="col-8">
                        <p>
                            ⼈数不利を覆す驚異的な⽴ち回りで、幾度となくチームを救ってきた、常勝チームの⽴役者。
                            試合終盤に魅せる冷静沈着な⽴ち回りが持ち味で、crowさえ⽣存していれば試合はどうなるかわからない。
                        </p>
                    </div>
                    
                    <div class="col">
                        <p>マウス　Logicool Gpro wireless SUPERLIGHT</p>
                        <img src="##">
                    </div>
                    <div class="col">
                        <p>ヘッドホン　Logicool G GPRO X</p>
                        <img src="##">
                    </div>
                    <div class="col">
                        <p>キーボード　Logicool G913</p>
                        <img src="##">
                    </div>
                </div>
        
                <div id="streamer_page" class="row border-bottom mt-5">
                    <h3>ストリーマー</h3>
                </div>
        
                <div class="row mt-5">
                    <div class="col-4">
                        <p>
                            Shroud
                        </p>
                        <img src="img/Valorant_Shroud.jpeg">
                    </div>
                    <div class="col-8">
                        <p>
                            Valorantのほか、CS:GOやPUBGをプレイすることで知られる今Twitchで最も勢いのあるストリーマー。
                            元e-Sportsプレイヤーであり、Twitchのフォロワー数は920万人を超える。
                        </p>
                    </div>
                    
                    <div class="col">
                        <p>マウス　Logicool G PRO Wireless</p>
                        <img src="##">
                    </div>
                    <div class="col">
                        <p>ヘッドホン　Logicool G PRO X</p>
                        <img src="##">
                    </div>
                    <div class="col">
                        <p>キーボード　Logicool G PRO X</p>
                        <img src="##">
                    </div>
                </div>
        
                <div class="row mt-5">
                    <div class="col-4">
                        <p>
                            StylishNoob
                        </p>
                        <img src="img/Valorant_StylishNoob.jpeg">
                    </div>
                    <div class="col-8">
                        <p>
                            「感謝します」というセリフが象徴的な日本を代表するベテランプレイヤー。
                            最近Zeta Diovisionに電撃加入し、視聴者を驚かせた。ゲームの腕前もさることながら、    
                            トークでも配信を盛り上げている。
                        </p>
                    </div>
                    
                    <div class="col">
                        <p>マウス　Logicool G903</p>
                        <img src="##">
                    </div>
                    <div class="col">
                        <p>ヘッドホン　SHURE SE215 Special Edition</p>
                        <img src="##">
                    </div>
                    <div class="col">
                        <p>キーボード　Logicool Logicool G512-LN</p>
                        <img src="##">
                    </div>
                </div>
                <div class="col my-5 text-center">
                    <a href="header">ページトップへ戻る</a>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
    
</body>

</html>