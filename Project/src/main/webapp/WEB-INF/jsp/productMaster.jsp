<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>商品管理</title>
	
    <link href="css/header.css" rel="stylesheet" type="text/css" />
    <link href="css/main.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        
</head>

<body>
	<div class="row sticky-top">
        <jsp:include page="header.jsp" />
    </div>

    <div id="container" class="container-fruid">
        <div class="row">
            <h4 class="my-3 p-3 border-bottom">商品管理ページ</h4>
        </div>
      	
      	<div>
      		<form class="col-8 mx-auto" action="ProductMasterServlet" method="post">
                <div class="mb-3">
                    <label for="productName" class="form-label">商品名</label>
                    <input type="text" class="form-control" name="name" placeholder="キーワードを入力">
                </div>
                <div class="row mb-3">
                <label for="productName" class="form-label">価格</label>
   					 <div class="col">
      					<input type="text" class="form-control" name="price" placeholder="最低価格">
    				 </div>
    				 <div class="col">
      					<input type="text" class="form-control" name="price2" placeholder="最高価格">
    				 </div>
  				</div>
                <div class="mb-3">
                    <select class="form-select" aria-label="productCategory" name="categoryId">
                        <option selected value="">カテゴリを選択</option>
                        <c:forEach var="category" items="${categoryList}">
                        	<option value="${category.categoryId}">${category.categoryName}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="mb-3">
                    <select class="form-select" aria-label="productCompany" name="companyId">
                        <option selected value="">メーカーを選択</option>
                        <c:forEach var="company" items="${companyList}">
                        	<option value="${company.companyId}">${company.companyName}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="d-grid gap-2 d-md-flex justify-content-md-end mb-5">
                	<button class="btn btn-primary me-md-2" type="submit">検索</button>
                </div>  
            </form>
      	</div>
      	
        <div class="table-responsive p-3">
            <table class="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">商品名</th>
                    <th scope="col">価格</th>
                    <th scope="col">カテゴリ</th>
                    <th scope="col">メーカー</th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody>
               		<c:forEach var="product" items="${productList}">
               			<tr>
                    		<td>${product.name}</td>
                    		<td>${product.price}円</td>
                    		<td>${product.categoryName}</td>
                    		<td>${product.companyName}</td>
                    		<td>
                       	 		<a class="btn btn-primary btn-sm" href="ProductUpdateServlet?id=${product.id}" role="button">更新</a>
                        		<a class="btn btn-danger btn-sm" href="ProductDeleteServlet?id=${product.id}" role="button">削除</a>
                    		</td>
                  		</tr>
               		</c:forEach>
                </tbody>
              </table>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW"
        crossorigin="anonymous"></script>
</body>
</html>