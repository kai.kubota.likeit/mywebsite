package model;

import java.io.Serializable;

public class Product implements Serializable{
	private int id;
	private int categoryId;
	private String categoryName;
	private int companyId;
	private String companyName;
	private String name;
	private int price;
	private String detail;
	private String imgPath;
	
	public Product(int id, String imgPath) {
		this.id = id;
		this.imgPath = imgPath;
	}
	
	public Product(String name, int price, String detail, String imgPath) {
		this.name = name;
		this.price = price;
		this.detail = detail;
		this.imgPath = imgPath;
	}
	
	public Product(int id, int categoryId, int companyId, String name, int price, String detail, String imgPath) {
		this.id = id;
		this.categoryId = categoryId;
		this.companyId = companyId;
		this.name = name;
		this.price = price;
		this.detail = detail;
		this.imgPath = imgPath;
	}
	
	public Product(int id, int categoryId, String categoryName, int companyId, String companyName, String name, int price, String detail, String imgPath) {
		this.id = id;
		this.categoryId = categoryId;
		this.categoryName = categoryName;
		this.companyId = companyId;
		this.companyName = companyName;
		this.name = name;
		this.price = price;
		this.detail = detail;
		this.imgPath = imgPath;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public int getCompanyId() {
		return companyId;
	}
	public void setCompany(int companyId) {
		this.companyId = companyId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public String getImgPath() {
		return imgPath;
	}
	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	

}
