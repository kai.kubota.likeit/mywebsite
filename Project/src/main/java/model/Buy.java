package model;

import java.io.Serializable;

public class Buy implements Serializable{
	private int id;
	private int userId;
	private int deliveryMethod;
	private int totalPrice;
	private String createDate;
	
	public Buy(int id, int userId, int deliveryMethod, int totalPrice, String createDate) {
		this.id = id;
		this.userId = userId;
		this.deliveryMethod = deliveryMethod;
		this.totalPrice = totalPrice;
		this.createDate = createDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getDeliveryMethod() {
		return deliveryMethod;
	}

	public void setDeliveryMethod(int deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}

	public int getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(int totalPrice) {
		this.totalPrice = totalPrice;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	
}
