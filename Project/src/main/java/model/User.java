package model;

public class User {
	private int id;
	private String email;
	private String name;
	private String adress;
	private String password;
	private String createDate;
	private String updateDate;
	
	//ログインIDをチェックするためのコンストラクタ
	public User(String email) {
		this.email = email;
	}
		
	// ログインセッションを保存するためのコンストラクタ
	public User(String email, String name) {
		this.email = email;
		this.name = name;
	}
	
	public User(int id, String email, String name, String adress) {
		this.id = id;
		this.email = email;
		this.name = name;
		this.adress = adress;
	}
	
	public User(String email, String name, String adress, String password) {
		this.email = email;
		this.name = name;
		this.adress = adress;
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}
	
}
