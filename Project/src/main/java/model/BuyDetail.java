package model;

public class BuyDetail {
	
	private int id;
	private int buyId;
	private int productId;
	
	public BuyDetail(int id, int buyId, int productId) {
		this.id = id;
		this.buyId = buyId;
		this.productId = productId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBuyId() {
		return buyId;
	}

	public void setBuyId(int buyId) {
		this.buyId = buyId;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}
	
}
