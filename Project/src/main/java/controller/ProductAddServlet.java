package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ProductDao;

@WebServlet("/ProductAddServlet")
public class ProductAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ProductAddServlet() {
        super();
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/productAdd.jsp");
		dispatcher.forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
	
		String strCategoryId = request.getParameter("categoryId");
		String strCompanyId = request.getParameter("companyId");
		String name = request.getParameter("name");
		String strPrice = request.getParameter("price");
		String detail = request.getParameter("detail");
		String imgPath = request.getParameter("imgPath");
		
		if ("".equals(strCategoryId) || "".equals(strCompanyId) || name.equals("") || "".equals(strPrice) || detail.equals("") || imgPath.equals("")) {

			request.setAttribute("errMsg", "入力された内容は正しくありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/productAdd.jsp");
			dispatcher.forward(request, response);
			return;
		}
		
		int categoryId = Integer.parseInt(strCategoryId);
		int companyId = Integer.parseInt(strCompanyId);
		int price = Integer.parseInt(strPrice);
		
		ProductDao productDao = new ProductDao();
		productDao.InsertProductInfo(categoryId, companyId, name, price, detail, imgPath);
		
		response.sendRedirect("ProductAddServlet");
	}

}
