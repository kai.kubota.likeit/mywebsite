package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class RegistConfirmServlet
 */
@WebServlet("/RegistConfirmServlet")
public class RegistConfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegistConfirmServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		HttpSession session = request.getSession();
		User registInfo = (User) session.getAttribute("registInfo");
		
		String email = registInfo.getEmail();
		String name = registInfo.getName();
		String adress = registInfo.getAdress();
		String password = registInfo.getPassword();
		
		UserDao userDao = new UserDao();
		userDao.InsertUserInfo(email, name, adress, password);
		
		User user = userDao.findByLoginInfo(email, password);
		session.removeAttribute("registInfo");
		session.setAttribute("userInfo", user);
		
		response.sendRedirect("HomeServlet");
	}

}
