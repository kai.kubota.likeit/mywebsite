package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ProductDao;
import model.Product;

@WebServlet("/ProductMasterServlet")
public class ProductMasterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public ProductMasterServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/productMaster.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		String name = request.getParameter("name");
		String strPrice = request.getParameter("price");
		String strPrice2 = request.getParameter("price2");
		String strCategoryId = request.getParameter("categoryId");
		String strCompanyId = request.getParameter("companyId");
		
		int price = 0;
		int price2 = 0;
		int categoryId = 0;
		int companyId = 0;
		
		if("".equals(name) && "".equals(strPrice) && "".equals(strPrice2) && "".equals(strCategoryId) && "".equals(strCompanyId)) {
			ProductDao productDao = new ProductDao();
			List<Product> productList = productDao.findAllProduct();
			request.setAttribute("productList", productList);
		}
		
		if(!("".equals(strPrice))) {
			price = Integer.parseInt(strPrice);
		}
		if(!("".equals(strPrice2))) {
			price2 = Integer.parseInt(strPrice2);
		}
		if(!("".equals(strCategoryId))) {
			categoryId = Integer.parseInt(strCategoryId);
		}
		if(!("".equals(strCompanyId))) {
			companyId = Integer.parseInt(strCompanyId);
		}
		
		ProductDao productDao = new ProductDao();
		List<Product> productList = productDao.findProduct(name, price, price2, categoryId, companyId);
		
		request.setAttribute("productList", productList);
		
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/productMaster.jsp");
		dispatcher.forward(request, response);
	}

}
