package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Category;
import model.Company;
import model.DeliveryMethod;
import model.Product;

public class ProductDao {
	
	public List<Product> pickUpNewItem() {
		Connection conn = null;
		List<Product> productList = new ArrayList<Product>();
		try {

			conn = DBManager.getConnection();

			String sql = "select * from product order by update_date desc";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			
			while (rs.next()) {
				int id = rs.getInt("id");
				String imgPath = rs.getString("image_path");
				Product product = new Product(id, imgPath);
				productList.add(product);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return productList;
	}
	
	public Product findByItemId(int id) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "select * from product inner join category on product.category_id = category.id inner join company on product.company_id = company.id where product.id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			
			int categoryId = rs.getInt("category_id");
			String categoryName = rs.getString("category");
			int companyId =rs.getInt("company_id");
			String companyName = rs.getString("company");
			String name = rs.getString("name");
			int price = rs.getInt("price");
			String detail = rs.getString("detail");
			String imgPath = rs.getString("image_path");
			
			return new Product(id, categoryId, categoryName, companyId, companyName, name, price, detail, imgPath);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	
	public List<Product> findByItemName(String name) {
		Connection conn = null;
		List<Product> productList = new ArrayList<Product>();
		try {

			conn = DBManager.getConnection();

			String sql = "select * from product where name like ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, "%" + name + "%");
			ResultSet rs = pStmt.executeQuery();

			while(rs.next()) {
				int idData = rs.getInt("id");
				int categoryId = rs.getInt("category_id");
				int companyId = rs.getInt("company_id");
				String nameData = rs.getString("name");
				int price = rs.getInt("price");
				String detail = rs.getString("detail");
				String imgPath = rs.getString("image_path");
				
				Product product = new Product(idData, categoryId, companyId, nameData, price, detail, imgPath);
				productList.add(product);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return productList;
	}
	
	public void InsertProductInfo(int categoryId, int companyId, String name, int price, String detail, String imgPath) {

		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "insert into product (category_id, company_id, name, price, detail, image_path, create_date, update_date) value (?, ?, ?, ?, ?, ?, now(), now());";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			
			pStmt.setInt(1, categoryId);
			pStmt.setInt(2, companyId);
			pStmt.setString(3, name);
			pStmt.setInt(4, price);
			pStmt.setString(5, detail);
			pStmt.setString(6, imgPath);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public List<Category> findCategory() {
		Connection conn = null;
		List<Category> categoryList = new ArrayList<Category>();
		try {

			conn = DBManager.getConnection();

			String sql = "select * from category order by id";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			
			while (rs.next()) {
				int categoryId = rs.getInt("id");
				String categoryName = rs.getString("category");
				Category category = new Category(categoryId, categoryName);
				categoryList.add(category);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return categoryList;
	}
	
	public List<Company> findCompany() {
		Connection conn = null;
		List<Company> companyList = new ArrayList<Company>();
		try {

			conn = DBManager.getConnection();

			String sql = "select * from company order by id";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			
			while (rs.next()) {
				int companyId = rs.getInt("id");
				String companyName = rs.getString("company");
				Company company = new Company(companyId, companyName);
				companyList.add(company);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return companyList;
	}
	
	public List<Product> findProduct(String name, int price, int price2, int categoryId, int companyId) {
		Connection conn = null;
		List<Product> productList = new ArrayList<Product>();

		try {

			conn = DBManager.getConnection();

			String sql = "select * from product inner join category on product.category_id = category.id inner join company on product.company_id = company.id";

			if (!(name.equals(""))) {
				sql += " where name like '%" + name + "%'";

			}
			if (price > 0) {
				sql += " and price >= '" + price + "'";

			}
			if (price2 > 0) {
				sql += " and price <= '" + price2 + "'";

			}
			if (categoryId > 0) {
				sql += " and category_id = '" + categoryId + "'";
			}
			if (companyId > 0) {
				sql += " and company_id = '" + companyId + "'";
			}

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				int category = rs.getInt("category_id");
				String categoryName = rs.getString("category");
				int company = rs.getInt("company_id");
				String companyName = rs.getString("company");
				String productName = rs.getString("name");
				int productPrice = rs.getInt("price");
				String detail = rs.getString("detail");
				String imgPath = rs.getString("image_path");

				Product product = new Product(id, category, categoryName, company, companyName, productName, productPrice, detail, imgPath);

				productList.add(product);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return productList;
	}
	
	public List<Product> findAllProduct() {
		Connection conn = null;
		List<Product> productList = new ArrayList<Product>();

		try {

			conn = DBManager.getConnection();

			String sql = "select * from product";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				int category = rs.getInt("category_id");
				String categoryName = rs.getString("category");
				int company = rs.getInt("company_id");
				String companyName = rs.getString("company");
				String productName = rs.getString("name");
				int productPrice = rs.getInt("price");
				String detail = rs.getString("detail");
				String imgPath = rs.getString("image_path");

				Product product = new Product(id, category, categoryName, company, companyName, productName, productPrice, detail, imgPath);

				productList.add(product);
				} 
			}catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
			return productList;
		}
	
	public void productDelete(int id) {
		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "delete from product where id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public void updateProduct (int id, int categoryId, int companyId, String name, int price, String detail, String imgPath) {
		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "update product set category_id = ?, company_id = ?, name = ?, price = ?, detail = ?, image_path = ?, update_date = now() where id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, categoryId);
			pStmt.setInt(2, companyId);
			pStmt.setString(3, name);
			pStmt.setInt(4, price);
			pStmt.setString(5, detail);
			pStmt.setString(6, imgPath);
			pStmt.setInt(7, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public List<DeliveryMethod> findDeliveryMethod() {
		Connection conn = null;
		List<DeliveryMethod> methodList = new ArrayList<DeliveryMethod>();
		try {

			conn = DBManager.getConnection();

			String sql = "select * from delivery_method order by id";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				int price = rs.getInt("price");
				DeliveryMethod method = new DeliveryMethod(id, name, price);
				methodList.add(method);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return methodList;
	}
}

