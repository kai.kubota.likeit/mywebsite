package dao;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.User;

public class UserDao {
	
	private String encodePassdigest(String password) {

		byte[] enclyptedHash = null;

		MessageDigest md5;

		try {
			md5 = MessageDigest.getInstance("MD5");
			md5.update(password.getBytes());
			enclyptedHash = md5.digest();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return bytesToHexString(enclyptedHash);
	}

	private String bytesToHexString(byte[] fromByte) {

		StringBuilder hexStrBuilder = new StringBuilder();
		for (int i = 0; i < fromByte.length; i++) {
			if ((fromByte[i] & 0xff) < 0x10) {
				hexStrBuilder.append("0");
			}
			hexStrBuilder.append(Integer.toHexString(0xff & fromByte[i]));
		}
		return hexStrBuilder.toString();
	}
	
	
	public User findByLoginInfo(String email, String password) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE email = ? and password = ?";

			String pass = encodePassdigest(password);

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, email);
			pStmt.setString(2, pass);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			
			int idData = rs.getInt("id");
			String emailData = rs.getString("email");
			String nameData = rs.getString("user_name");
			String adressData = rs.getString("adress");
			
			return new User(idData, emailData, nameData, adressData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	
	public void InsertUserInfo(String email, String name, String adress, String password) {

		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "insert into user(email, user_name, adress, password, create_date, update_date) value ( ?, ?, ?, ?, now(), now())";

			String pass = encodePassdigest(password);

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, email);
			pStmt.setString(2, name);
			pStmt.setString(3, adress);
			pStmt.setString(4, pass);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public User findByEmail(String email) {
		Connection conn = null;

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE email = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, email);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			String emailData = rs.getString("email");
			return new User(emailData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	
	public User updateUserInfo(int id, String email, String name, String adress) {
		Connection conn = null;

		try {

			conn = DBManager.getConnection();
			
			String sql = "update user set email = ?, user_name = ?, adress = ?, update_date = NOW() where id = ?";
			
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, email);
			pStmt.setString(2, name);
			pStmt.setString(3, adress);
			pStmt.setInt(4, id);

			pStmt.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return new User(id, email, name, adress);
	}
	
	public void userDelete(String email) {
		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "delete from user where email = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, email);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
